# Docker Infrastructure

Ansible collection for managing a docker compose infrastructure

## Visuals

Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation (initial setup)

Installing Ansible via [pip and venv](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-in-a-virtual-environment-with-pip)
& lock the dependencies variables.

```bash
# Was required in Ubuntu 20LTS via WSL2
sudo apt install python3.8-venv

python3 -m venv ansible6  # Create a virtual env if one does not already exist
source ansible6/bin/activate   # Activate the virtual environment
pip install --upgrade setuptools wheel pip
pip install -r pip_requirements/dev-requirements.txt

# Backup the list of package dependencies
pip freeze > pip_requirements/current-requirements.txt
```

## Upgrade Ansible

Save current dependencies versions, remove the `ansible6/` dir & [reinitialize the python env with desired Ansible version](#initial-setup-control-node).

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
